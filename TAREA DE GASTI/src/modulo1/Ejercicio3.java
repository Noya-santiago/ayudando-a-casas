package modulo1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Tecla de Escape \t\t\t Significado");
		System.out.println("\n\\n \t\t\t \t\t Significa nueva linea");
		System.out.println("\\t \t\t\t\t\t Significa un Tab de espacio");
		System.out.println("\\\" \t\t\t\t\t es para poner doble comillas por ejemplo \"belencita\"");
		System.out.println("\\\\ \t\t\t\t\t se utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\  ");
		System.out.println("\\' \t\t\t\t\t se utiliza para las \\' (comilla simple) para escribir por ejemplo \'princesita\' ");	
	}

}
